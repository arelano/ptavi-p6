#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import os


class RTPHandler(socketserver.DatagramRequestHandler):
    """
    RPT server class
    """

    def handle(self):
        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read().decode('utf-8')
            METHODS = 'INVITE', 'ACK', 'BYE'

            # Si no hay más líneas salimos del bucle infinito
            if not line:
                break
            data = line.split(" ")
            method = data[0]
            print("Llega " + line)
            ID = data[1]
            RECEIVER = ID[4:ID.find("@")]
            IP = ID[ID.find("@")+1:]
            if method == 'INVITE':
                reply_invite = "SIP/2.0 100 Trying\r\n\r\n"
                reply_invite += "SIP/2.0 180 Ringing\r\n\r\n"
                reply_invite += "SIP/2.0 200 OK\r\n\r\n"
                self.wfile.write(bytes(reply_invite, 'utf-8'))
            elif method == 'ACK':
                aEjecutar = ('./mp32rtp -i 127.0.0.1 -p 23032 < ' + AUDIO_FILE)
                print("ejecutando " + aEjecutar)
                os.system(aEjecutar)
            elif method == 'BYE':
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            elif method not in METHODS:
                self.wfile.write(b"SIP/2.0 405 Method Not Allowed")
        else:
            self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")


if __name__ == "__main__":
    try:
        IP = sys.argv[1]
        PORT = int(sys.argv[2])
        AUDIO_FILE = sys.argv[3]
    except (IndexError, ValueError):
        sys.exit("Usage: python3 server.py IP port audio_file")
    serv = socketserver.UDPServer((IP, PORT), RTPHandler)
    print("Listening")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
