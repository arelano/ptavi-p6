"""
Programa cliente que abre un socket a un servidor
"""
# python3 client.py Metodo batman@193.147.73.20:5555

import socket
import sys

# Cliente UDP simple.

# Dirección IP del servidor.

METOD = sys.argv[1]
ID = sys.argv[2]
RECEIVER = ID[0:ID.find("@")]
IP = ID[ID.find("@")+1:ID.find(":")]
SIPport = int(ID[ID.find(":")+1:])
MSGtail = " sip:" + RECEIVER + "@IP SIP/2.0"


# Contenido que vamos a enviar

DICCMETODS = {'INVITE': 'INVITE' + MSGtail,
              'ACK': 'ACK' + MSGtail,
              'BYE': 'BYE' + MSGtail}

if METOD in DICCMETODS:
    LINE = DICCMETODS[METOD]
# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((IP, SIPport))

        print("Enviando: " + LINE)
        my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
        data = my_socket.recv(1024)

        print('Recibido -- ', data.decode('utf-8'))
        answer = data.decode('utf-8')
        if answer == ("SIP/2.0 100 Trying\r\n\r\n" +
                      "SIP/2.0 180 Ringing\r\n\r\n" +
                      "SIP/2.0 200 OK\r\n\r\n"):
            line = DICCMETODS['ACK']
            print("Enviando: " + line)
            my_socket.send(bytes(line, 'utf-8') + b'\r\n')

        print("Terminando socket...")

print("Fin.")
